package bookmanager.service;

import bookmanager.dao.BookDao;
import bookmanager.model.Book;

import java.util.List;

/**
 * Created by Молния on 25.02.2017.
 */
public interface BookService extends BookDao {
}
